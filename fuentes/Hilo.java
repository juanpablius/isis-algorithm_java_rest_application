package services;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Semaphore;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;


public class Hilo extends Thread{

	private int numMensajes = 10;
	private int id = 0;
	private String servidor = null;
	List<Mensaje> colaMensajes;
	int orden = 0;
	CyclicBarrier barrera = new CyclicBarrier(3);
	int dniLocal = 0;
	String flag = null;
	int numNotificaciones = 0;
	private Semaphore sem = null;
	
	 private CountDownLatch controlador = new CountDownLatch(3);  
	
	public Hilo (int _id, String _servidor, int _dniLocal, int _numMensajes)
	{
		
		this.id = _id;
		this.servidor = _servidor;
		this.dniLocal = _dniLocal;
		this.numMensajes = _numMensajes;
		
		sem = new Semaphore (0);
		colaMensajes =  Collections.synchronizedList (new ArrayList<Mensaje>());
	}
	
	
	

	 
	 

	
	 
	public void run()
	{
		 
        iniciarMicola ();
      
     //   muestraMiCola (servidor, id);
      //  flag = notificarEspera ();
        
        /*if (flag.compareTo("nonada") == 0)	espera ();*/
        
        dormirTiempoAleatorio();
         

        
       
        multidifundir (id, dniLocal);
        
        muestraMiCola (servidor, id);
       
        
        dormirTiempoAleatorio();
        
        
        	
        enviarPropuesta ();
        
    
        
        dormirTiempoAleatorio();
         
   //   muestraMiCola (servidor, id);
        System.out.println("\n\n El tigre  " + id +  "duerme") ;
        espera ();//*/
        System.out.println("\n\n El tigre  " + id +  "se despierta") ;  
        escribirLog(1);
        escribirLog(2);
       // finalizar ();
        
		System.out.println("\n\nHilo    : " + id + " ha pasado a mejor vida...");
		
	}
	




	public void espera() {
		// TODO Auto-generated method stub
		
		try {
			sem.acquire();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/*synchronized (getClass())
	     {
	    	  try {
	    			getClass().wait();
	    		} catch (InterruptedException e1) {
	    			// TODO Auto-generated catch block
	    			e1.printStackTrace();
	    		}
	     }*/
	}



	public void notificar ()
	{
		
		sem.release(2);
		/*
			synchronized (getClass())
			{
				
				getClass().notify();
				getClass().notify();
				
				
			
			}*/
		
		
	
		
	}
	public String notificarEspera() {
		// TODO Auto-generated method stub
		
		
		Client client=ClientBuilder.newClient();
		String uriServer =   servidor +  "/ISIS2";
	    URI uri = UriBuilder.fromUri(uriServer).build();
	    WebTarget target = client.target(uri);
	     int i;
	    String cas = new String();
	    
	    
	    cas =target.path("servicio").path("notificarEspera").
	    			queryParam("hilo", id).
					request(MediaType.TEXT_PLAIN).get(String.class);
	    
	    return cas;
	}



	public   void iniciarMicola ()
	{
		Client client=ClientBuilder.newClient();
		String uriServer =   servidor +  "/ISIS2";
	    URI uri = UriBuilder.fromUri(uriServer).build();
	    WebTarget target = client.target(uri);
	     int i;
	    String cas = new String();
	    
	    for ( i = 0; i < numMensajes; i ++)	
        {
	    
	    
		   cas =target.path("servicio").path("iniciarCola").
				   		queryParam("origen", id). 
				   		queryParam("numeroSecuencia", i).
				   		queryParam("dniLocal", dniLocal).
				   		request(MediaType.TEXT_PLAIN).get(String.class);
		 
		 
		 
		   
        } 
	    
	}
	
	public   void multidifundir (int proceso, int _dniLocal)
	{
		/*1-  Crear un identificador �nico de mensaje mediante el n�mero de mensaje y el identificador de proceso
        
    	
    	/*2-  Multidifundir el mensaje �Pxx nnn� donde xx es el identificador de proceso y nnn el n�mero de mensaje*/
    	 
    	
    	
    	
    	
    	/*3-  Dormir un tiempo aleatorio entre 1.0 y 1.5s*/
    	//dormirTiempoAleatorio();
		
		Client client=ClientBuilder.newClient();
		String uriServer =   servidor +  "/ISIS2";
	    URI uri = UriBuilder.fromUri(uriServer).build();
	    WebTarget target = client.target(uri);
	     int i;
	    String cas = new String();
	   
	     
	    	for ( i = 0; i < numMensajes; i ++)	
	        {
		    
		    
			   cas =target.path("servicio").path("multidifundirMensaje").
					   						queryParam("origen", id). 
					   						queryParam("numeroSecuencia", i).
					   						queryParam("dniLocal", dniLocal).
					   						request(MediaType.TEXT_PLAIN).get(String.class);
			 
			  dormirTiempoAleatorio();
			
			
	        } 

				
	}
	
	
	public void muestraMiCola (String _servidor, int _id)
	{
		Client client=ClientBuilder.newClient();
		String uriServer =  servidor +  "/ISIS2";
	    URI uri = UriBuilder.fromUri(uriServer).build();
	    WebTarget target = client.target(uri);
	    String cas;
	    
	    cas =target.path("servicio").path("muestraColaMensajes").
									 queryParam("numServer", servidor).
									 queryParam("idThread", id).
									 queryParam("DNI", dniLocal).
									 request(MediaType.TEXT_PLAIN).get(String.class);
	    
	    
	}
	public void escribirEnFichero ()
	{
		
		Client client=ClientBuilder.newClient();
		String uriServer =  servidor +  "/ISIS2";
	    URI uri = UriBuilder.fromUri(uriServer).build();
	    WebTarget target = client.target(uri);
		
		/*Escribir en el mismo fichero */
		FileWriter fichero = null;
        PrintWriter pw = null;
        
		
		
		try {
			fichero = new FileWriter("C:\\Users\\juanp\\Desktop\\" + servidor + "P" + id + ".txt", true);
			pw = new PrintWriter(fichero, true);
		
			
			for (int i = 0 ; i < colaMensajes.size() ; i ++)
			{
			pw.println("unProceso:  "  + colaMensajes.get(i).proceso + "Mensaje" + colaMensajes.get(i).id);
			
			}		   
						
			
		} catch (IOException e1) {	
			// TODO Auto-generated catch blockm  
			e1.printStackTrace();
		}finally {
	           try {
	               // Nuevamente aprovechamos el finally para 
	               // asegurarnos que se cierra el fichero.
	               if (null != fichero)
	                  fichero.close();
	               } catch (Exception e2) {
	                  e2.printStackTrace();
	               }
		}
	
	}
	public synchronized  String devuelveCadena (int _id , int _secuencia)
	{
		Mensaje m = new Mensaje();
		m.setMessage(_id, _secuencia, orden,null);
		 colaMensajes.add(m);
		
		 String var = new String();
		 var = "servidor:" + servidor+ "P" + String.valueOf(m.proceso) +  "secuencia " +String.valueOf(m.id);
		 
			FileWriter fichero = null;
	        PrintWriter pw = null;
	        
			
			
			try {
				fichero = new FileWriter("C:\\Users\\juanp\\Desktop\\" + servidor + "P0" + id + ".txt", true);
				pw = new PrintWriter(fichero, true);
			
				
				 
				pw.println("Servidor " + servidor + "  _Proceso:  "  + _id+ "_Mensaje" + _secuencia);
				
				 		
				
			} catch (IOException e1) {	
				// TODO Auto-generated catch blockm  
				e1.printStackTrace();
			}finally {
		           try {
		               // Nuevamente aprovechamos el finally para 
		               // asegurarnos que se cierra el fichero.
		               if (null != fichero)
		                  fichero.close();
		               } catch (Exception e2) {
		                  e2.printStackTrace();
		               }
			}
		 
		 return var;
	}
	public void dormirTiempoAleatorio()
	{
		double tiempo;
		Random aleatorio;
		
		
		aleatorio = new Random();
		tiempo = 1 + (1.5 -1) * aleatorio.nextDouble();
		try 
    	{
			Thread.sleep  ((long) (tiempo * 1000));
			//Thread.sleep  (1750);
		} catch (InterruptedException e)
    	{
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
	}
	public void enviarPropuesta ()
	{
		Client client    =	 ClientBuilder.newClient();
		String uriServer =   servidor +  "/ISIS2";
		URI uri 		 = UriBuilder.fromUri(uriServer).build();
	    WebTarget target = client.target(uri);
	    String cas = new String ();
	    
	    
	    cas =target.path("servicio").path("enviarPropuestas").
	    							queryParam("hilo", id). 
	    							queryParam("DNI", dniLocal).
	    							request(MediaType.TEXT_PLAIN).get(String.class);
	    
	    
	    System.out.println("\n------------- \n He enviado ya todas las propuestas : -> " + id + " R: " + cas);
	}
	
	 public  void escribirLog (int tipo)
	 {
		 Client client=ClientBuilder.newClient();
			String uriServer =   servidor +  "/ISIS2";
		    URI uri = UriBuilder.fromUri(uriServer).build();
		    WebTarget target = client.target(uri);
		     int i;
		    String cas = new String();
		    
		   if (tipo == 1)
		   {
			   cas =target.path("servicio").path("escribirFichero").
		    			queryParam("hilo", id).
		    			queryParam("DNI", dniLocal).
						request(MediaType.TEXT_PLAIN).get(String.class);     
		   }
		   
		   
		   if (tipo == 2)
		   {
			   cas =target.path("servicio").path("escribirFichero2").
		    			queryParam("hilo", id).
		    			queryParam("DNI", dniLocal).
						request(MediaType.TEXT_PLAIN).get(String.class);     
		   }
		    
	 }
	
	public void finalizar() {
		// TODO Auto-generated method stub
		Client client=ClientBuilder.newClient();
		String uriServer =   servidor +  "/ISIS2";
	    URI uri = UriBuilder.fromUri(uriServer).build();
	    WebTarget target = client.target(uri);
	     int i;
	    String cas = new String();
	    
	    
	    cas =target.path("servicio").path("finalizarVariables").
	    			queryParam("hilo", id).
					request(MediaType.TEXT_PLAIN).get(String.class);
	}
}
