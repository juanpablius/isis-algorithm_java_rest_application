package services;


import java.net.URI;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import java.io.Serializable;  
import javax.xml.bind.annotation.XmlElement; 
import javax.xml.bind.annotation.XmlRootElement; 
 

public class Main   {
	public static void main(String args[])
		{
		/* Main: Lanzar� los tres servidores y se encargar� de hacer la asignaci�n de los procesos a los servidores*/
		
		
		
		 /*----------------1) Configuraci�n de los servidores ------------------------------------------*/
		
		// Servidor1
		String servidor1 = "http://172.20.7.62:8080";
		String servidor2 = "http://172.20.1.16:8080";
		String servidor3 = "http://172.20.3.55:8080";
		
		
		Client client1=ClientBuilder.newClient();
		Client client2=ClientBuilder.newClient();
		Client client3=ClientBuilder.newClient();
		 
		
		
		String uriServer1 =  servidor1 +  "/ISIS2";
		String uriServer2 =  servidor2 +  "/ISIS2";
		String uriServer3 =  servidor3 +  "/ISIS2";
		
		
		
		java.net.URI uri1 = UriBuilder.fromUri(uriServer1).build();
	    WebTarget target1 = client1.target(uri1);
	    
	    java.net.URI uri2 = UriBuilder.fromUri(uriServer2).build();
	    WebTarget target2 = client2.target(uri2);
	    
	    java.net.URI uri3 = UriBuilder.fromUri(uriServer3).build();
	    WebTarget target3 = client3.target(uri3);
	    
	     
	    
	    // Servidor1
	     
	    int proceso1 = 1;
	    int proceso2 = 2;
	    
	    // Servidor2
	     
	     
	    int proceso3 = 3;
	    int proceso4 = 4;
	    
	    // Servidor3
	  
	     
	    int proceso5 = 5;
	    int proceso6 = 6;
	    
	    
	    
 
	    target1.path("servicio").path("creaHilos").
		queryParam("servidor1", servidor1).//Servidor1
		queryParam("servidor2", servidor2).
		queryParam("servidor3", servidor3).
		queryParam("idproceso1", proceso1).// Proceso1
		queryParam("idproceso2", proceso2).// Proceso2
		request(MediaType.TEXT_PLAIN).get(String.class);
	    
	    
	    target2.path("servicio").path("creaHilos").
		queryParam("servidor1", servidor2). //Servidor2
		queryParam("servidor2", servidor1).
		queryParam("servidor3", servidor3).
		queryParam("idproceso1", proceso3).// Proceso3	    					
		queryParam("idproceso2", proceso4).// Proceso4
		request(MediaType.TEXT_PLAIN).get(String.class);
		
	    target3.path("servicio").path("creaHilos").
		queryParam("servidor1", servidor3).//Servidor3
		queryParam("servidor2", servidor2).
		queryParam("servidor3", servidor1).
		queryParam("idproceso1", proceso5).//Proceso5
		queryParam("idproceso2", proceso6).//Proceso6
		request(MediaType.TEXT_PLAIN).get(String.class);
		
		
 
		
	    try 
    	{
			Thread.sleep  (3000);
		} catch (InterruptedException e)
    	{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		}
}
