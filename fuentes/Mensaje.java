package services;

import java.awt.geom.Rectangle2D;
import java.util.Comparator;
import java.util.Date;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

public class Mensaje implements Comparable<Mensaje>{
   
	int id;
	int proceso;
	private int reloj;
	boolean isApproved;
	String numServidor;
	int totalProposals;
	private String messageId;
	
	
	
	
	public Mensaje ()
	{
		this.id = -1;
		this.proceso = -1;
		this.reloj = -1;
		this.isApproved = false;
		this.numServidor = new String ();
		this.totalProposals = 0;
		this.messageId = new String();
	}
	
	public void setMessage (int _proceso, int _id, int _reloj, String _numservidor)
	{
		proceso = _proceso;
		id = _id;
		reloj = _reloj;
		numServidor = _numservidor;
		messageId = "P" + String.valueOf(_proceso) + String.valueOf(_id);
	}

	@Override
	public int compareTo(Mensaje o) {
		return (this.reloj- o.reloj );
	}

	String getMessageId() {
		return messageId;
	}

	void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	int getReloj() {
		return reloj;
	}

	void setReloj(int reloj) {
		this.reloj = reloj;
	}
 
}
