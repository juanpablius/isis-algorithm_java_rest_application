package services;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
 
import javax.ws.rs.core.UriBuilder;

import com.sun.org.apache.xerces.internal.util.URI;

 

@Singleton 
@Path("servicio")	//ruta a la clase
public class Servicio {
	
	/* Cada servidor se dennotar� as� mismo como servidor 1*/
	int numMensajes = 100;
	
	String servidor1 = null;
	String servidor2  = null;
	String servidor3 = null;
 
	List<Mensaje> colaInicial1;
	List<Mensaje> colaInicial2;
	 
	List<Mensaje> colaDesordenada1;
	List<Mensaje> colaDesordenada2;
	
	List <Mensaje> colaGenerica1;
	List <Mensaje> colaGenerica2;
	
	ArrayList <Integer> numDefs = null;
	
	
	
	int contadorTotal;
	
	int contadorColaGenerica = 1;
	int contadorCola1 = 1;
	int contadorCola2 = 1;
	
	int contadorCola3 = 0;
	
	int id1;
	int id2;
	
	
	
	int countdown = 1;
	Hilo h1;
	Hilo h2;
	
	int dniLocal1 = 1 ;
 	int dniLocal2 = 2 ;
	
	Integer numDef1 = null;
	Integer numDef2 = null;
	
	int numNotificacionesEspera = 0;
	int numProcesos  = 6;
	int flag = 0;
	
	 
	String debugIPServer1  = "http://172.20.7.62:8080";
	String debugIPServer2  = "http://172.20.1.16:8080";
	String debugIPServer3  = "http://172.20.3.55:8080";
	
	int numNotificaciones = 0;
	
	//colaGenerica = null;
	//colaGenerica = new ArrayList<>();
	
	
	
	
	@GET			//tipo de petici�n HTTP
	@Produces(MediaType.TEXT_PLAIN)	//tipo de texto devuelto
	@Path("creaHilos")	//ruta al m�todo
	public String creaHilos(@QueryParam(value = "servidor1") String _servidor1,
							@QueryParam(value = "servidor2") String _servidor2,
							@QueryParam(value = "servidor3") String _servidor3,	
							@QueryParam(value = "idproceso1") int _idProceso1,
							@QueryParam(value = "idproceso2") int _idProceso2)
	 
	{
		numDefs = new ArrayList<>();
		numDefs.add(0);
		numDefs.add(0);
		
		
		contadorCola1 = 1;
		contadorCola2 = 1; 
		contadorColaGenerica = 1;
		colaInicial1 = null;
		colaInicial1 = new ArrayList<>();
		
		colaInicial2 = null;
		colaInicial2 = new ArrayList<>();
		
		colaDesordenada1 = null;
		colaDesordenada1 = new ArrayList<>();
		
		colaDesordenada2 = null;
		colaDesordenada2 = new ArrayList<>();
		
		colaGenerica1 = null;
		colaGenerica1 = new ArrayList<>();
		
		colaGenerica2 = null;
		colaGenerica2 = new ArrayList<>();
		
		numDef1 = new Integer(0);
		numDef2 = new Integer(0); 
		
		System.out.println("\nIniciando servicio nuevo " + _servidor1);
	 	
		// 1) Identificar que servidor soy yo 
	 	servidor1 = _servidor1;
	 	servidor2 = _servidor2;
	 	servidor3 = _servidor3;
	 	
	 	// 2) Asignar la id a los procesos que tendr� este servidor
	 	id1 = _idProceso1;
	 	id2 = _idProceso2;
	 	
	 	 
	 	
	 	// 3) Launch!
		 h1 = new Hilo (id1, servidor1, dniLocal1, numMensajes);
		 h2 = new Hilo (id2, servidor1, dniLocal2, numMensajes);
		 
		 
		 
		h1.start();
		h2.start();
		
		
		// 4) Sleep 
		
	 
		
		
		
		// 5) Return al main
		String retorno = "Hilos: " + id1 + " , " + id2 + " creados en servidor " + servidor1;
		return retorno;
		}
	
	
	@GET			//tipo de petici�n HTTP
	@Produces(MediaType.TEXT_PLAIN)	//tipo de texto devuelto
	@Path("iniciarCola")	//ruta al m�todo	
	public synchronized String iniciarCola (@QueryParam(value = "origen") int _origen,
								 			@QueryParam(value = "numeroSecuencia") int _numeroSecuencia,
								 			@QueryParam(value = "dniLocal") int _dniLocal)
	{	
		String serverActual = null;
		
		
		 if (_origen == 1 || _origen == 2)	serverActual = debugIPServer1;
		 if (_origen == 4 || _origen == 3)	serverActual = debugIPServer2;
		 if (_origen == 5 || _origen == 6)	serverActual = debugIPServer3;
		
		 Mensaje m = new Mensaje ();
 
		 
		 if (dniLocal1 == _dniLocal)
		 {
			 m.setMessage(_origen, _numeroSecuencia, contadorCola1,serverActual);
			 colaInicial1.add(m);
			 //colaGenerica1.add(m);
			 contadorCola1++;
			 
		 }
		 
		 
		 if (dniLocal2 == _dniLocal)
		 {
			 m.setMessage(_origen, _numeroSecuencia, contadorCola2,serverActual);
			 colaInicial2.add(m);
			 //colaGenerica2.add(m);
			 contadorCola2++;
			 
		 }
		 
		 
	
		return "colaCargada" ;
	}
	

	
	@GET			//tipo de petici�n HTTP
	@Produces(MediaType.TEXT_PLAIN)	//tipo de texto devuelto
	@Path("multidifundirMensaje")	//ruta al m�todo	
	public  String multidifundirMensaje (@QueryParam(value = "origen") int _origen,
									     @QueryParam(value = "numeroSecuencia") int _numeroSecuencia)
	{
		
		
		int origen = _origen;
		int numeroSecuencia = _numeroSecuencia;
		String varF = new String();
		String var1 = new String();
		String var2 = new String();
		String var3 = new String();
		
		String uriServer1 =  servidor1 +  "/ISIS2";
		String uriServer2 =  servidor2 +  "/ISIS2";
		String uriServer3 =  servidor3 +  "/ISIS2";
		
		Client client1=ClientBuilder.newClient();
		Client client2=ClientBuilder.newClient();
		Client client3=ClientBuilder.newClient();
		Client client4=ClientBuilder.newClient();;
		
		java.net.URI uri1 = UriBuilder.fromUri(uriServer1).build();
		java.net.URI uri2 = UriBuilder.fromUri(uriServer2).build();
		java.net.URI uri3 = UriBuilder.fromUri(uriServer3).build();
		
		
		WebTarget target1 = client1.target(uri1);
		WebTarget target2 = client2.target(uri2);
		WebTarget target3 = client3.target(uri3);
		
		
		System.out.println("Multidifundiendo :" + _origen + _numeroSecuencia + " en " + uri2);
		if  (servidor1 == null || servidor2== null  || servidor3 == null)
		{
			// Comprobaci�n de errores
			System.out.println("ERROR: " + "servidor1 " + servidor1 + " servidor2 " + servidor2 + " servidor3 " + servidor3);
			return "mierda";
		}
	 
		var1 = target1.path("servicio").path("recibirMensaje").
										queryParam("origen", origen).
										queryParam("numeroSecuencia", numeroSecuencia).
										request(MediaType.TEXT_PLAIN).get(String.class) ;

		var2 = target2.path("servicio").path("recibirMensaje").
										queryParam("origen", origen).
										queryParam("numeroSecuencia", numeroSecuencia).
										request(MediaType.TEXT_PLAIN).get(String.class) ;
		
		 
		var3 = target3.path("servicio").path("recibirMensaje").
								 		queryParam("origen", origen).
								 		queryParam("numeroSecuencia", numeroSecuencia).
								 		request(MediaType.TEXT_PLAIN).get(String.class) ;
	
		  
		  
		
			
			return "LOCURA";
		
	}
	
	
	@GET			//tipo de petici�n HTTP
	@Produces(MediaType.TEXT_PLAIN)	//tipo de texto devuelto
	@Path("recibirMensaje")	//ruta al m�todo	
	public   String recibirMensaje (@QueryParam(value = "origen") int _origen,
			  						@QueryParam(value = "numeroSecuencia") int _numeroSecuencia)
	
	{
		String _var ="recibirMensaje"; 
		System.out.println("Peticion de P" + _origen + _numeroSecuencia + " en seRveR " + servidor1 );
		
		 String serverActual = null;
		 
		 // Identifico el servidor en el que estoy
		 if (_origen == 1 || _origen == 2)	serverActual = debugIPServer1;
		 if (_origen == 4 || _origen == 3)	serverActual = debugIPServer2;
		 if (_origen == 5 || _origen == 6)	serverActual = debugIPServer3;
		 
		 
		 
		 Mensaje m = new Mensaje ();
		 Mensaje v = new Mensaje ();
		 
		 m.setMessage(_origen, _numeroSecuencia, contadorColaGenerica,serverActual);
		 v.setMessage(_origen, _numeroSecuencia, contadorColaGenerica,serverActual);
		 
		 colaGenerica1.add(m);
		 colaGenerica2.add(m);
		 
		 colaDesordenada1.add(v);
		 colaDesordenada2.add(v);
		 
		 contadorColaGenerica++;

		 
	
		return (_var);
		
	}
	
	@GET			//tipo de petici�n HTTP
	@Produces(MediaType.TEXT_PLAIN)	//tipo de texto devuelto
	@Path("muestraColaMensajes")	//ruta al m�todo	
	public  String muestraColaMensajes (@QueryParam(value = "numServer") String _numServer,
										@QueryParam(value = "idThread")  int _idThread,
										@QueryParam(value = "DNI")  int _dni)
			 
									  
	{
		
		System.out.print("Imprimiendo cola del server: " + _numServer + "\n");
		
		if (dniLocal1 == _dni  )	imprimeCola (colaGenerica1,_idThread );
		if (dniLocal2 == _dni  )	imprimeCola (colaGenerica2,_idThread );
		
		return "eso es todo migos";
	}
	
	
	 
	public synchronized void imprimeCola (List <Mensaje> cola, int _idTh)
	  
	{
		cola  =   Collections.synchronizedList(cola );
	 
		 synchronized (cola)
		 {
		 Iterator<Mensaje> iterator = cola.iterator(); 
	      
		 	while (iterator.hasNext()) 
		 	{
	    	     Mensaje m =  iterator.next();
	    	     System.out.println( "P" + m.proceso + m.id +
	    	    		 			" orden "      + m.getReloj()   +
	    	    		 			" propuestas " + m.totalProposals +
	    	    		 			" estado "     + m.isApproved    );
	    	    		// 			" en servidor" + m.numServidor + "at  Thread "  + _idTh );
	       }// while
	       
		 }// Synchronized
	          
	        
	} // function

	

	
	
	 
	@GET			//tipo de petici�n HTTP
	@Produces(MediaType.TEXT_PLAIN)	//tipo de texto devuelto
	@Path("enviarPropuestas")	//ruta al m�todo	
	public  String enviarPropuestas 	(@QueryParam(value = "hilo") int _hilo,
										 @QueryParam(value = "DNI") int _dni	)
	// Este m�todo tomar� cada mensaje que haya en su servidor y  llamar�  a la funci�n RecibirPropuestas de todos los servidores
	{
		//System.out.println("Servidor: " +   servidor1 + "  ejecutando  enviarPropuestas "   );
		
		String var1 = new String();
		String messageToSendId = new String();
		String serverToSend =  null;
		int messageToSendOrder = -1;
		String uriServer1  = new String();
		Client client1=ClientBuilder.newClient();
		
		List <Mensaje> cola = null;
		
		if (dniLocal1 == _dni)	cola = colaGenerica1;
		if (dniLocal2 == _dni)	cola = colaGenerica2;
		if (cola == null)	return "ERROR";
		
		for  (int i = 0; i < cola.size() ; i++)
		{
			//synchronized(getClass())
			//{
			

			 messageToSendId    = cola.get(i).getMessageId();
			 serverToSend       = cola.get(i).numServidor;
			 messageToSendOrder = cola.get(i).getReloj();
			 
			 System.out.println("\n*-*-*-*-*-* Hilo: " 	+ _hilo
					 					+ "Enviando propuesta a  : " + serverToSend 
					 					+ " mensaje : " + messageToSendId 
					 					+ " orden " 	+ messageToSendOrder
					 					+ "Iteraci�n :" + i);
			 
			 
			uriServer1 =  serverToSend +  "/ISIS2";
			java.net.URI uri1 = UriBuilder.fromUri(uriServer1).build();
			WebTarget target1 = client1.target(uri1);
			
			
			var1 = target1.path("servicio").path("recibirPropuestas").
										    queryParam("messageId", messageToSendId).
										    queryParam("orderProposed", messageToSendOrder).
										    queryParam("hilo", _hilo).
										    request(MediaType.TEXT_PLAIN).get(String.class);
			
			
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			}
		
		//}
	 
			
		return var1;
	}
		
	
	
	
		@GET			//tipo de petici�n HTTP
		@Produces(MediaType.TEXT_PLAIN)	//tipo de texto devuelto
		@Path("recibirPropuestas")	//ruta al m�todo	
	public    String recibirPropuestas (@QueryParam(value = "messageId") String _messageId,
									    @QueryParam(value = "orderProposed") int _orderProposed,
									    @QueryParam(value = "hilo") int _hilo)
	
		
		{
			 
				recibirPropuestasEn (colaGenerica1, _messageId, _orderProposed, _hilo );
				recibirPropuestasEn (colaGenerica2, _messageId, _orderProposed, _hilo );

			 
		// synchrokiller
		  
		return "nonada";
	}
		
		

									    
		
		public  void recibirPropuestasEn(List<Mensaje> cola, String _messageId, int _orderProposed,
				int _hilo) {


			int previousOrder;
			String var1 = new String();
			
			 System.out.println("\nRecibiendo propuesta en Servidor : " + servidor1 
					 			+ " MessageId : "     +   _messageId 
					 			+ " orden propuesto " + _orderProposed  
					 			+ " por el hilo "     +     _hilo);

			
			  
						
						
			// Busca el mensaje en su cola
			for (int i = 0; i < cola.size(); i ++)
			{
				
					
				
				
				if (cola.get(i).getMessageId().compareTo(_messageId) == 0) 
				{
					 System.out.println("\nEl mensaje :  " +  _messageId + " tiene un orden :  " + cola.get(i).getReloj() +
							 "Y un orden   propuesto por " + _orderProposed + " el hilo " + _hilo);
					 
					 
					previousOrder = cola.get(i).getReloj();
					
					if (_orderProposed > previousOrder) 
					{
						cola.get(i).setReloj(_orderProposed + 1);
					}
					
					else cola.get(i).setReloj(cola.get(i).getReloj() + 1);
					
					int temp = cola.get(i).totalProposals;
					temp++;
					
					cola.get(i).totalProposals++;// = temp;
					
					System.out.println( "\nSe han recibido "                + cola.get(i).totalProposals										
										+ "  propuestas  para el  mensaje " +  cola.get(i).getMessageId()			  
										+ " orden aCtual "                  +  cola.get(i).getReloj() 
										+ " estado  "                       + cola.get(i).isApproved );
										
					 
					if (cola.get(i).totalProposals == 6 * 2 ) {
						cola.get(i).isApproved = true;
						String messageToSendId = cola.get(i).getMessageId();
						int messageToSendOrder = cola.get(i).getReloj();
						multidifundirAcuerdo(messageToSendId, messageToSendOrder);
					}
					
					
				     
					
				}// if compare to
				
		 	
				
			}// for
			
		}


		public void multidifundirAcuerdo(String messageApprovalId, int messageApprovalOrder) {
			
			String var1 = new String();
			String var2 = new String();
			String var3 = new String();
			
			String uriServer1 = servidor1 +  "/ISIS2";
			String uriServer2 = servidor2 +  "/ISIS2";
			String uriServer3 = servidor3 +  "/ISIS2";
			
			Client client1=ClientBuilder.newClient();
			Client client2=ClientBuilder.newClient();
			Client client3=ClientBuilder.newClient();
			
			java.net.URI uri1 = UriBuilder.fromUri(uriServer1).build();
			java.net.URI uri2 = UriBuilder.fromUri(uriServer2).build();
			java.net.URI uri3 = UriBuilder.fromUri(uriServer3).build();
			
			
			
			WebTarget target1 = client1.target(uri1);
			WebTarget target2 = client2.target(uri2);
			WebTarget target3 = client3.target(uri3);
			
			System.out.println("Multidifundiendo acuerdo :: " + messageApprovalId + " en orden " + messageApprovalOrder);
			
			var1 = target1.path("servicio").path("recibirAcuerdo").
					queryParam("messageId", messageApprovalId).
					queryParam("finalOrderProposed", messageApprovalOrder).
					queryParam("servidorOrigen", servidor1).
					request(MediaType.TEXT_PLAIN).get(String.class);
			
			var1 = target2.path("servicio").path("recibirAcuerdo").
					queryParam("messageId", messageApprovalId).
					queryParam("finalOrderProposed", messageApprovalOrder).
					queryParam("servidorOrigen", servidor1).
					request(MediaType.TEXT_PLAIN).get(String.class);
			
			
			var1 = target3.path("servicio").path("recibirAcuerdo").
					queryParam("messageId", messageApprovalId).
					queryParam("finalOrderProposed", messageApprovalOrder).
					queryParam("servidorOrigen", servidor1).
					request(MediaType.TEXT_PLAIN).get(String.class);
			
			

		}
				
		@GET			//tipo de petici�n HTTP
		@Produces(MediaType.TEXT_PLAIN)	//tipo de texto devuelto
		@Path("recibirAcuerdo")	//ruta al m�todo	
		public  String recibirAcuerdo (@QueryParam(value = "messageId") String _messageId,
									   @QueryParam(value = "finalOrderProposed") int _finalOrderProposed,
									   @QueryParam(value = "servidorOrigen") String _servidorOrigen)
		
		{
			
			recibirAcuerdoEn (colaGenerica1, _messageId, _finalOrderProposed, _servidorOrigen, 0);
			recibirAcuerdoEn (colaGenerica2, _messageId, _finalOrderProposed, _servidorOrigen, 1 );
			
			return "nonada";
		}	
 
		public   void recibirAcuerdoEn(List<Mensaje> cola, String _messageId, int _finalOrderProposed,String _servidorOrigen, int  indice) {
			
			int flag = 0;
			int df;
			
			
			 
			
			
			for (int i=0; i < cola.size(); i++) {
				
					
				
				if (cola.get(i).getMessageId().compareTo(_messageId) == 0) 
				{
						
					cola.get(i).setReloj(_finalOrderProposed);
					cola.get(i).isApproved = true;
					System.out.println("\n\nMENSAJE DEFINITIVO " + _messageId + " ORDEN "      + _finalOrderProposed  + 
										" EN SERVIDORactual "    + servidor1  + " REMITENTE "  + _servidorOrigen);
					flag = 1 ;
					numDefs.set(indice,  numDefs.get(indice) + 1); 
				}
				
				
			}
			
			 
			
			 
			
			if (numDefs.get(indice) == (6 * numMensajes ))
			{
				
				System.out.println("\n\n\n!!!!!!!!!!!!!!!!!IMPRIMIENDO  COLA ORDENADA!!!!!!!!!!!");
				 
					Collections.sort(cola, Comparator.comparing(Mensaje::getReloj)
						    .thenComparing(Mensaje::getMessageId));
				 
					
					numNotificaciones++;
					System.out.println("\n\n>>>>>>>>>>>>>>>>>>>>>>>>>>><< num notif " + numNotificaciones);
					if (numNotificaciones == 2)
					{
						Hilo h0;
						 h0 = new Hilo (id2, servidor1, 0 , 0);
						 h0.notificar ();
						 h1.notificar();
						 h2.notificar();
					}
				
					 
				
				
			   
				
				
				if (servidor1.equals(debugIPServer1))	imprimeCola(cola, 1);
				
				if (servidor1.equals(debugIPServer2))	imprimeCola(cola, 2);
				
				if (servidor1.equals(debugIPServer3))	imprimeCola(cola, 3);
				
				
				
			 
			}
			
			
		

			else
			{
				 
				/*
				System.out.println("\n!!!!!!!!!!!!!!!!!Cola No ordenada --- numDefinitivos :" + numDefs.get(indice) );
				
				if (servidor1.equals(debugIPServer1))	imprimeCola(cola, 1);
				
				if (servidor1.equals(debugIPServer2))	imprimeCola(cola, 2);
				
				if (servidor1.equals(debugIPServer3))	imprimeCola(cola, 3);
				
				*/ 
				
				
				 
			}
			 
			 
			
			
			
		}


		@GET			//tipo de petici�n HTTP
		@Produces(MediaType.TEXT_PLAIN)	//tipo de texto devuelto
		@Path("finalizarVariables")	//ruta al m�todo	
		public    String finalizarVariables (@QueryParam(value = "hilo") int _hilo)
	{
			flag++;
			if (flag == 2)
			{
				 servidor1 = null;
				 servidor2  = null;
				  servidor3 = null;
			 
 
				  
				
				contadorTotal = 0;
				contadorCola1 = 1;
				contadorCola2 = 1;
				contadorCola3 = 0;
				
				id1 = 0;
				id2 = 0;
				
				countdown = 1;
				
				contadorCola1 = 1;
				colaGenerica1 = null;
			 
				
				numDef1 = 0;
				numDef2 = 0;
				numNotificacionesEspera = 0;
				numProcesos  = 6;
				flag = 0;
			}
			return "nonada";
	}
 
		
		
		
		@GET			//tipo de petici�n HTTP
		@Produces(MediaType.TEXT_PLAIN)	//tipo de texto devuelto
		@Path("escribirFichero")	//ruta al m�todo	
		public  synchronized  String escribirFichero (@QueryParam(value = "hilo") int _hilo,
											      @QueryParam(value = "DNI") int _dni)
	{
			FileWriter fichero = null;
	        PrintWriter pw = null;
	        
	        List <Mensaje> cola = null;
			
			if (dniLocal1 == _dni)	cola = colaGenerica1;
			if (dniLocal2 == _dni)	cola = colaGenerica2;
			
	        
	        
	        
			try {
				fichero = new FileWriter("/home/i0966436/Z/" + "P" + _hilo + ".txt");
				pw = new PrintWriter(fichero);
			
				
				for (int i = 0 ; i < cola.size() ; i ++)
				{
				pw.println("P" + cola.get(i).proceso + cola.get(i).id  + " orden " + cola.get(i).getReloj());
				
				}		   
							
				
			} catch (IOException e1) {	
				// TODO Auto-generated catch blockm  
				e1.printStackTrace();
			}finally {
		           try {
		               // Nuevamente aprovechamos el finally para 
		               // asegurarnos que se cierra el fichero.
		               if (null != fichero)
		                  fichero.close();
		               } catch (Exception e2) {
		                  e2.printStackTrace();
		               }
			}
			
			return "nonada";
	}
		
		

		@GET			//tipo de petici�n HTTP
		@Produces(MediaType.TEXT_PLAIN)	//tipo de texto devuelto
		@Path("escribirFichero2")	//ruta al m�todo	
		public  synchronized  String escribirFichero2 (@QueryParam(value = "hilo") int _hilo,  
												   @QueryParam(value = "DNI") int _dni)
	{
			FileWriter fichero = null;
	        PrintWriter pw = null;
	        
	        List <Mensaje> cola = null;
			
			if (dniLocal1 == _dni)	cola = colaDesordenada1;
			if (dniLocal2 == _dni)	cola = colaDesordenada2;
	        
			try {
				fichero = new FileWriter("/home/i0966436/Z/" + "Desordenado_P" + _hilo + ".txt");
				pw = new PrintWriter(fichero);
			
				
				for (int i = 0 ; i < cola.size() ; i ++)
				{
				pw.println("P" + cola.get(i).proceso + cola.get(i).id  + " orden " + cola.get(i).getReloj());
				
				}		   
							
				
			} catch (IOException e1) {	
				// TODO Auto-generated catch blockm  
				e1.printStackTrace();
			}finally {
		           try {
		               // Nuevamente aprovechamos el finally para 
		               // asegurarnos que se cierra el fichero.
		               if (null != fichero)
		                  fichero.close();
		               } catch (Exception e2) {
		                  e2.printStackTrace();
		               }
			}
			
			return "nonada";
	}
		
		@GET			//tipo de petici�n HTTP
		@Produces(MediaType.TEXT_PLAIN)	//tipo de texto devuelto
		@Path("notificarEspera")	//ruta al m�todo	
		public   String notificarEspera (@QueryParam(value = "hilo") int _hilo)
		{
			String var1 = new String();
			String var2 = new String();
			String var3 = new String();
			
			String uriServer1 = servidor1 +  "/ISIS2";
			String uriServer2 = servidor2 +  "/ISIS2";
			String uriServer3 = servidor3 +  "/ISIS2";
			
			Client client1=ClientBuilder.newClient();
			Client client2=ClientBuilder.newClient();
			Client client3=ClientBuilder.newClient();
			
			java.net.URI uri1 = UriBuilder.fromUri(uriServer1).build();
			java.net.URI uri2 = UriBuilder.fromUri(uriServer2).build();
			java.net.URI uri3 = UriBuilder.fromUri(uriServer3).build();
			
			
			
			WebTarget target1 = client1.target(uri1);
			WebTarget target2 = client2.target(uri2);
			WebTarget target3 = client3.target(uri3);
			
			System.out.println(" Env�o de notificacion de espera de : " + _hilo);
			
			var1 = target1.path("servicio").path("recibirNotifEspera").
					queryParam("hilo", _hilo).
					request(MediaType.TEXT_PLAIN).get(String.class);
			
			var2 = target2.path("servicio").path("recibirNotifEspera").
					queryParam("hilo", _hilo).
					request(MediaType.TEXT_PLAIN).get(String.class);
			
			var3 = target3.path("servicio").path("recibirNotifEspera").
					queryParam("hilo", _hilo).
					request(MediaType.TEXT_PLAIN).get(String.class);
			
			 
			
			
			return "nonada";
		}
		
		
		
		
		@GET			//tipo de petici�n HTTP
		@Produces(MediaType.TEXT_PLAIN)	//tipo de texto devuelto
		@Path("recibirNotifEspera")	//ruta al m�todo	
		public   String recibirNotifEspera (@QueryParam(value = "hilo") int _hilo)
		{
			
			 numNotificacionesEspera++;
			 System.out.println("Servidor : " + servidor1 +  " recibo notificacion de " + _hilo + " numNotificEnServer " + numNotificacionesEspera );
			 if (numNotificacionesEspera == 6)
			 {
				
				 Hilo h0;
				 h0 = new Hilo (id2, servidor1, 0 , 0);
				 h0.notificar ();
				 h1.notificar();
				 h2.notificar();
				 return "si";
			 }
			
			 
			return "nonada";
		}
		
		@GET			//tipo de petici�n HTTP
		@Produces(MediaType.TEXT_PLAIN)	//tipo de texto devuelto
		@Path("muyayo")	//ruta al m�todo
		public String muyayo()	//el m�todo debe retornar String
			{
			
			System.out.println("Imprime");
			return " Se ha actualizado el servidor : 90000";
			}
		
		
}